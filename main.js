const CONFIG = {
	SettingButton:   false,  // 是否显示设置按钮
	TimeTextRefresh: true,   // 是否使用当前时间
	SettingsEnabled: true,   // 是否应用用户设置
	URLArgsEnabled:  true,   // 是否应用url参数
}

window.addEventListener('load', function() {
	const storageKey = 'texts';
	const Elms = {
		bg_up:      $('#bg_up'),
		bg_down:    $('#bg_down'),
		content:    $('#content'),
		name:       $('#name'),
		number:     $('#number'),
		company:    $('#company'),
		scan_time:  $('#scan_time'),
		location:   $('#location'),
		pass_time1: $('#pass_time1'),
		pass_time2: $('#pass_time2'),
		settings:   $('#settings')
	};
	const Info = {
		bgColor:   getComputedStyle(Elms.bg_up).backgroundColor,
		setText:   Elms.settings.innerText
	};
	const ids = ['name', 'number', 'company', 'location'/*, 'pass_time1', 'pass_time2'*/];

	accessCheck();
	editor();
	automator();

	function editor() {
		if (!CONFIG.SettingButton) {
			Elms.settings.style.display = 'none';
			return false;
		}

		const editables = ids.map((id)=>(Elms[id]));
		for (const elm of editables) {
			elm.addEventListener('click', edit);
		}

		let editmode = false;
		Elms.settings.addEventListener('click', function() {
			(editmode ? stopEdit : startEdit)();
		});

		return true;

		function edit(e) {
			if (!editmode) {return false;}

			const elm = e.target;
			const text = prompt('请设置{T}'.replace('{T}', elm.title || '本项内容'), elm.innerText);
			if (text !== null) {
				const config = read();
				if (text !== '') {
					elm.innerText = text;
					config[elm.id] = text;
				} else {
					elm.innerText = '（未知）';
					delete config[elm.id];
				}
				save(config);
			}
		}

		function startEdit() {
			editmode = true;
			Elms.bg_up.style.backgroundColor = 'green';
			Elms.settings.innerText = 'Edit Mode';
		}

		function stopEdit() {
			editmode = false;
			Elms.bg_up.style.backgroundColor = Info.bgColor;
			Elms.settings.innerText = Info.setText;
		}
	}

	function automator() {
		CONFIG.TimeTextRefresh && timeSettings();
		CONFIG.SettingsEnabled && userSettings();
		CONFIG.URLArgsEnabled  && urlSettings();


		// Write time-based-texts
		function timeSettings() {
			const d = new Date();
			const date = '{Y}-{M}-{D}'
				.replace('{Y}', fillNumber(d.getFullYear(), 4))
				.replace('{M}', fillNumber(d.getMonth()+1,  2))
				.replace('{D}', fillNumber(d.getDate(),     2));
			const time = '{H}:{M}:{S}'
				.replace('{H}', fillNumber(d.getHours(),    2))
				.replace('{M}', fillNumber(d.getMinutes(),  2))
				.replace('{S}', fillNumber(d.getSeconds(),  2));
			Elms.scan_time.innerText = '{D} {T}'.replace('{D}', date).replace('{T}', time);
			Elms.pass_time1.innerText = '出校时间 {D}'.replace('{D}', date);
			Elms.pass_time2.innerText = '入校时间 {D}'.replace('{D}', date);
		}

		// Write user-set-texts
		function userSettings() {
			const config = read();
			for (const id in config) {
				if (config.hasOwnProperty(id) && ids.includes(id)) {
					Elms[id].innerText = config[id];
				}
			}
		}

		// Write url arguments-based-texts
		function urlSettings() {
			for (const id of ids) {
				const text = getUrlArgv(id);
				text && Elms[id] && (Elms[id].innerText = decodeURIComponent(text));
			}
		}
	}

	function read() {
		return JSON.parse(localStorage.getItem(storageKey) || '{}');
	}

	function save(config) {
		localStorage.setItem(storageKey, JSON.stringify(config));
	}

	function accessCheck() {
		const access = (localStorage.getItem(storageKey) || (CONFIG.URLArgsEnabled && location.search)) ? true : false;
		access && (document.body.style.display = 'block');
		return access || (function() {
			//location.href = 'https://pyudng.github.io/';
			document.write('<div style="text-align: center;"><span style="font-size: 5vmin;">403 No Access</span></div>');
			return false;
		}) ();
	}
});

function $(s) {
	return document.querySelector(s);
}

// Fill number text to certain length with '0'
function fillNumber(number, length) {
    let str = String(number);
    for (let i = str.length; i < length; i++) {
        str = '0' + str;
    }
    return str;
}

// Get a url argument from lacation.href
// also recieve a function to deal the matched string
// returns defaultValue if name not found and null if defaultValue also not found
// Args: name, dealFunc=(function(a) {return a;}), defaultValue=null
function getUrlArgv(details) {
    typeof(details) === 'string'    && (details = {name: details});
    typeof(details) === 'undefined' && (details = {});
    if (!details.name) {return null;};

    const url = details.url ? details.url : location.href;
    const name = details.name ? details.name : '';
    const dealFunc = details.dealFunc ? details.dealFunc : ((a)=>{return a;});
    const defaultValue = details.defaultValue ? details.defaultValue : null;
	const matcher = new RegExp(name + '=([^&]+)');
	const result = url.match(matcher);
	const argv = result ? dealFunc(result[1]) : defaultValue;

	return argv;
}